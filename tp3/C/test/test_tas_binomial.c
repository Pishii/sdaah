#include <stdlib.h>
#include <stdio.h>
#include "structures/tas_binomial.h"

int test_fusion();
int test_insertion(heap_b** h,int* tab, int n);
int test_extraction();
int comparaison_tas_binomial (heap_b* a, heap_b* b);
int comparaison_noeud (binomial_node* a, binomial_node* b);

int main(){
    /*Pour tester nos fonctions on construit trois tas
     * de références, les trois tas possédent 10 éléments.
     * 
     * Les degrées des différents sous-tas dépend seulement
     * du nombre de noeud du tas principal.
     *
     * La valeur des clés de chaque noeud dépend de l'ordre d'insertion, 
     * et la relation d'ordre entre chaque clé de chaque noeud.
     *  
     * Le tas ref_10 est le tas binomial de référence pour 10
     * éléments inséré dans l'ordre croissant des clés.
     *
     * Le tas ref_10_inv est le tas binomial de référence pour
     * 10 éléments inséré dans l'ordre décroissant des clés.
     *
     */
    
    
    
    int tab_insertion_2[10] = {1,2,3,4,5,6,7,8,9,10};
    int tab_insertion_3[10] = {10,9,8,7,6,5,4,3,2,1};
    
    /***************************************************/
    heap_b* test_ref = create_heap(); 
    heap_b* test_inv = create_heap(); 

    test_insertion(&test_ref, tab_insertion_2, 10);
    test_insertion(&test_inv, tab_insertion_3, 10);

    /***************************************************/ 
    
    heap_b ref_10;
    
    binomial_node elem_1;
    elem_1.key = 1;

    binomial_node elem_2;
    elem_2.key = 2;

    binomial_node elem_3;
    elem_3.key = 3;

    binomial_node elem_4;
    elem_4.key = 4;

    binomial_node elem_5;
    elem_5.key = 5;

    binomial_node elem_6;
    elem_6.key = 6;

    binomial_node elem_7;
    elem_7.key = 7;

    binomial_node elem_8;
    elem_8.key = 8;

    binomial_node elem_9;
    elem_9.key = 9;

    binomial_node elem_10;
    elem_10.key =10;

    elem_9.brother = &elem_1;
    elem_9.child = &elem_10;
    elem_9.father = NULL;

    elem_10.brother = NULL;
    elem_10.child = NULL;
    elem_10.father = &elem_9;

    elem_1.brother = NULL;
    elem_1.child = &elem_5;
    elem_1.father = NULL;

    elem_5.brother = &elem_3;
    elem_5.child = &elem_7;
    elem_5.father = &elem_1;

    elem_3.brother = &elem_2;
    elem_3.child = &elem_4;
    elem_3.father = &elem_1;

    elem_2.brother = NULL;
    elem_2.child = NULL;
    elem_2.father = &elem_1;

    elem_7.brother = &elem_6;
    elem_7.child = &elem_8;
    elem_7.father = &elem_5;

    elem_6.brother = NULL;
    elem_6.father = &elem_5;
    elem_6.child = NULL;

    elem_8.brother = NULL;
    elem_8.child = NULL;
    elem_8.father = &elem_7;
    
    ref_10.nodes = &elem_9;
    ref_10.nb_head = 10;

    /******************************************************/


    heap_b ref_inv_10;

    binomial_node inv_1;
    inv_1.key = 1;
    
    binomial_node inv_2;
    inv_2.key = 2;

    binomial_node inv_3;
    inv_3.key = 3;

    binomial_node inv_4;
    inv_4.key = 4;

    binomial_node inv_5;
    inv_5.key = 5;

    binomial_node inv_6;
    inv_6.key = 6;

    binomial_node inv_7;
    inv_7.key = 7;

    binomial_node inv_8;
    inv_8.key = 8;

    binomial_node inv_9;
    inv_9.key = 9;

    binomial_node inv_10;
    inv_10.key = 10;


    inv_1.brother = &inv_3;
    inv_1.child = &inv_2;
    inv_1.father = NULL;

    inv_2.brother = NULL;
    inv_2.child = NULL;
    inv_2.father = &inv_1;

    inv_3.brother = NULL;
    inv_3.child = &inv_7;
    inv_3.father = NULL;

    inv_7.brother = &inv_5;
    inv_7.child = &inv_9;
    inv_7.father = &inv_3;

    inv_9.brother = &inv_8;
    inv_9.child = &inv_10;
    inv_9.father = &inv_7;

    inv_5.brother = &inv_4;
    inv_5.child = &inv_6;
    inv_5.father = &inv_3;

    inv_6.brother = NULL;
    inv_6.child = NULL;
    inv_6.father = &inv_5;

    inv_4.brother = NULL;
    inv_4.child = NULL;
    inv_4.father = &inv_3;

    ref_inv_10.nodes = &inv_1;
    ref_inv_10.nb_head = 10;
    /********************************************/
    show_heap(test_ref);
    show_heap(test_inv);

    /*********************************************/
    
    if(comparaison_tas_binomial(test_ref, &ref_10)<0)
        printf("non pour ref_10\n");
    else
        printf("ok pour ref_10\n");

    if(comparaison_tas_binomial(test_inv, &ref_inv_10)<0)
        printf("non pour ref_inv_10\n");
    else
        printf("ok pour ref_inv_10\n");

     
    destroy_heap(test_ref);
    destroy_heap(test_inv);




    return 0;
}

int test_insertion(heap_b** h, int* tab, int n){
    int error_code = 0;
    for(int i=0; i<n; i++){
        error_code = insert_binomial_element(NULL, tab[i], h); 

        if(error_code !=0){
            return error_code; 
        }
    }

    return error_code;
}

int comparaison_tas_binomial (heap_b* a, heap_b* b){
   return comparaison_noeud(a->nodes, b->nodes);

}

int comparaison_noeud (binomial_node* a, binomial_node* b){
   /*pour comparer 2 tas binomial il faut faire un
    * parcours sur le graphe qu'est le tas.
    * La méthode de parcours importe peu.
    * Ici il s'agit d'un parcours en profondeur d'abord.*/

    binomial_node** nodes_a;
    binomial_node** nodes_b;

   int pos_a = 0;
   int pos_b = 0;

   binomial_node* stack_head_a;
   binomial_node* stack_head_b;

   if(a == NULL || b == NULL)
       return -1;
   if(a->degree != b->degree)
       return -1;
    
    nodes_a = (binomial_node**) malloc(10*sizeof(binomial_node*));
    nodes_b = (binomial_node**) malloc(10*sizeof(binomial_node*));
    
    /*la valeur 10 à été calculé en avance sur les jeu de test
     * c'est pas très propre mais tant pis c'est pour le test
     */

    nodes_a[pos_a] = a;
    nodes_b[pos_b] = b;
    

    while(pos_a < 0 || pos_b < 0){
        if(nodes_a[pos_a]->key != nodes_b[pos_b]->key)
            return -1;

        stack_head_a = nodes_a[pos_a];
        pos_a--;

        stack_head_b = nodes_b[pos_b];
        pos_b--;

        if(stack_head_a->child !=NULL){
            pos_a ++; 
            nodes_a[pos_a] = stack_head_a->child;
        }
        if(stack_head_a->brother != NULL){
            pos_a ++;
            nodes_a[pos_a] = stack_head_a->brother;
        }

        if(stack_head_b->child!=NULL){
            pos_b ++;
            nodes_b[pos_b] = stack_head_b->child;
        }

        if(stack_head_b->brother!=NULL){
            pos_b ++;
            nodes_b[pos_b] = stack_head_b->brother;
        }
    }

    return 0;


}
