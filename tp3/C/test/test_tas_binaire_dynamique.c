#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include "structures/tas_binaire_dynamique.h"
#include "analysis/analyzer.h"

int tab[1000];
int main(){
    analyzer_t* ajout_croissant_binaire = analyzer_create();
    analyzer_t* ajout_decroissant_binaire = analyzer_create();
    analyzer_t* ajout_aleatoire_binaire = analyzer_create();
	analyzer_t* ajout_extract_binaire = analyzer_create();

    tas_binaire_dynamique_t* tas_ajout_croissant=  creerTasBinaire(0);
    tas_binaire_dynamique_t* tas_ajout_decroissant=  creerTasBinaire(1000);
	tas_binaire_dynamique_t* tas_ajout_aleatoire=  creerTasBinaire(0);
	tas_binaire_dynamique_t* tas_ajout_extract=  creerTasBinaire(0);
	 

    struct timespec after, before;
    clockid_t clk_id = CLOCK_REALTIME;
    srand(11509205); 

    printf("\nScénario avec ajout croissant\n");	
    for(int i = 1; i < 1000; i++){
        clock_gettime(clk_id, &before); 
        inserer(tas_ajout_croissant,i);
        clock_gettime(clk_id, &after);
        analyzer_append(ajout_croissant_binaire, after.tv_nsec - before.tv_nsec);
    }
    printf("temps total : %f \n", get_total_cost(ajout_croissant_binaire));
    printf("temps amorti : %f \n", get_amortized_cost(ajout_croissant_binaire));
    printf("temps moyen : %f \n", get_average_cost(ajout_croissant_binaire));
    printf("variance : %f \n", get_variance(ajout_croissant_binaire));
    printf("écart_type : %f \n", get_standard_deviation(ajout_croissant_binaire));
    save_values(ajout_croissant_binaire, "./ajout_croissant_binaire_dyn.plot");
	detruireTasBinaire(tas_ajout_croissant);
	analyzer_destroy(ajout_croissant_binaire);

    printf("\nScénario avec ajout decroissant\n");
    for(int i = 999; i>0;i--){
           clock_gettime(clk_id, &before); 
        inserer(tas_ajout_decroissant,i);
        clock_gettime(clk_id, &after);
        analyzer_append(ajout_decroissant_binaire, after.tv_nsec - before.tv_nsec);
    }

    printf("temps total : %f \n", get_total_cost(ajout_decroissant_binaire));
    printf("temps amorti : %f \n", get_amortized_cost(ajout_decroissant_binaire));
    printf("temps moyen : %f \n", get_average_cost(ajout_decroissant_binaire));
    printf("variance : %f \n", get_variance(ajout_decroissant_binaire));
    printf("écart_type : %f \n", get_standard_deviation(ajout_decroissant_binaire));
    save_values(ajout_decroissant_binaire, "./ajout_decroissant_binaire_dyn.plot");
	detruireTasBinaire(tas_ajout_decroissant);
	analyzer_destroy(ajout_decroissant_binaire);

    printf("\nScénario avec ajout aléatoire\n");
    
    for(int i = 0; i<1000; i++)
    {
        tab[i] = rand();
    }
    for(int i = 0; i<1000; i++){
        clock_gettime(clk_id, &before); 
        inserer(tas_ajout_aleatoire, tab[i]);
        clock_gettime(clk_id, &after);
        analyzer_append(ajout_aleatoire_binaire, after.tv_nsec - before.tv_nsec);
    }
        
    printf("temps total : %f \n", get_total_cost(ajout_aleatoire_binaire));
    printf("temps amorti : %f \n", get_amortized_cost(ajout_aleatoire_binaire));
    printf("temps moyen : %f \n", get_average_cost(ajout_aleatoire_binaire));
    printf("variance : %f \n", get_variance(ajout_aleatoire_binaire));
    printf("écart_type : %f \n", get_standard_deviation(ajout_aleatoire_binaire));
    save_values(ajout_aleatoire_binaire, "./ajout_aleatoire_binaire_dyn.plot");
	detruireTasBinaire(tas_ajout_aleatoire);
	analyzer_destroy(ajout_aleatoire_binaire);
	
	printf("\nScénario avec ajout et extraction min\n");
    for(int i = 0; i<1000; i++){
        clock_gettime(clk_id, &before); 
		inserer(tas_ajout_extract, tab[i]);
        clock_gettime(clk_id, &after);
        analyzer_append(ajout_extract_binaire, after.tv_nsec - before.tv_nsec);
    }
     for(int i = 0; i<1000; i++){
        clock_gettime(clk_id, &before); 
		extraireMin(tas_ajout_extract);
        clock_gettime(clk_id, &after);
        analyzer_append(ajout_extract_binaire, after.tv_nsec - before.tv_nsec);
    }
        
    printf("temps total : %f \n", get_total_cost(ajout_extract_binaire));
    printf("temps amorti : %f \n", get_amortized_cost(ajout_extract_binaire));
    printf("temps moyen : %f \n", get_average_cost(ajout_extract_binaire));
    printf("variance : %f \n", get_variance(ajout_extract_binaire));
    printf("écart_type : %f \n", get_standard_deviation(ajout_extract_binaire));
    save_values(ajout_extract_binaire, "./ajout_extract_binaire_dyn.plot");
	detruireTasBinaire(tas_ajout_extract);
	analyzer_destroy(ajout_extract_binaire);
    
    return 0;
}
