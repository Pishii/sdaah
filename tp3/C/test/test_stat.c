#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include "structures/tas_binomial.h"
#include "analysis/analyzer.h"

int main(){
    analyzer_t* ajout_bino = analyzer_create();
    analyzer_t* retrait_bino = analyzer_create();
    analyzer_t* bino = analyzer_create();

    heap_b* insert_stat= create_heap();
    heap_b* merge_stat = create_heap();

    int ret;

    struct timespec before, after;
    srand(11513398); 

    printf("Scénario avec ajout seulement\n");
    for(int i = 0; i < 1000000; i++){
        clock_gettime(CLOCK_REALTIME, &before);
        insert_binomial_element(NULL, rand(), &insert_stat);
        clock_gettime(CLOCK_REALTIME, &after);
        analyzer_append(ajout_bino, after.tv_nsec - before.tv_nsec);
    }

    printf("temps total : %f \n", get_total_cost(ajout_bino));
    printf("temps moyen : %f \n", get_average_cost(ajout_bino));
    printf("variance : %f \n", get_variance(ajout_bino));
    printf("écart_type : %f \n", get_standard_deviation(ajout_bino));

    save_values(ajout_bino, "./ajout_bino.plot");

    printf("Scénario avec retrait seulement\n");
    for(int j = 0; j<(1000000-1);j++){
        clock_gettime(CLOCK_REALTIME, &before);
        extract_min_key(&insert_stat, &ret);
        clock_gettime(CLOCK_REALTIME, &after);
        analyzer_append(retrait_bino, after.tv_nsec - before.tv_nsec); 
    }


    printf("temps total : %f \n", get_total_cost(retrait_bino));
    printf("temps moyen : %f \n", get_average_cost(retrait_bino));
    printf("variance : %f \n", get_variance(retrait_bino));
    printf("écart_type : %f \n", get_standard_deviation(retrait_bino));

    save_values(retrait_bino, "./retrait_bino.plot");

    printf("Scénario avec ajout, retrait et fusion\n");
    destroy_heap(insert_stat);
    insert_stat = create_heap();

    for(int k = 0; k<1000; k++){
        clock_gettime(CLOCK_REALTIME, &before);
        insert_binomial_element(NULL, rand(), &insert_stat);
        insert_binomial_element(NULL, rand(), &merge_stat);
        
        if(rand()%50 == 0) {
            merge_stat = binomial_merge(&merge_stat, &insert_stat);
            insert_stat = create_heap();
        }
        clock_gettime(CLOCK_REALTIME, &after);

        analyzer_append(bino, after.tv_nsec - before.tv_nsec); 

    }


    printf("temps total : %f \n", get_total_cost(bino));
    //printf("temps amorti : %f \n", get_amortized_cost(bino));
    printf("temps moyen : %f \n", get_average_cost(bino));
    printf("variance : %f \n", get_variance(bino));
    printf("écart_type : %f \n", get_standard_deviation(bino));

    save_values(bino, "./bino.plot");

    analyzer_destroy(ajout_bino);
    analyzer_destroy(retrait_bino);
    analyzer_destroy(bino);
    
    return 0;
}

