#include <stdlib.h>
#include<stdio.h>
#include "structures/tas_binomial.h"


void destroy_heap_nodes(binomial_node **b);
void destroy_childs(binomial_node *b);
void destroy_brothers(binomial_node *b);
void binomial_bind(binomial_node *b, binomial_node* c);

heap_b* create_heap(){
    heap_b* ret;

    ret = (heap_b*) malloc(sizeof(heap_b));
    
    if(ret != NULL){ 
        ret->nodes=NULL;
        ret->nb_head = 0; 
    }

    return ret;
}


int insert_binomial_element(void* e,int key ,heap_b** h){
    heap_b* insert_buffer = *h;
    if(h == NULL||*h == NULL)
        return -4;

    heap_b* insert_result = NULL;
    
    heap_b* element_heap = create_heap();
    if(element_heap == NULL)
        return -1;
    
    binomial_node* element_node;

    element_node = malloc(sizeof(binomial_node));

    if(element_node == NULL){
        free(element_heap);
        return -2;
    }
    
    element_node->content = e;
    element_node->key = key;
    element_node->degree = 1;
    element_node->father = NULL;
    element_node->brother = NULL;
    element_node->child = NULL; 

    element_heap->nodes = element_node;
    element_heap->nb_head = 1;

    insert_result = binomial_merge(&element_heap, &insert_buffer);

    if(insert_result == NULL){
        free(element_heap);
        return -3;
    }

    *h = insert_result;

    return 0;

}


void binomial_bind(binomial_node* b, binomial_node* c){
    /*Fait de b le fils de c*/
    if(b == NULL)
        return;

    b->father = c;
    b->brother = c->child;
    
    c->child = b;
    c->degree++;

}

binomial_node* convert_binomial_heap_linked_list(heap_b* a, heap_b* b){
    binomial_node* ret = NULL;
    binomial_node* lastest = NULL;
    binomial_node* bro_follower_a = a->nodes;
    binomial_node* bro_follower_b = b->nodes;

    if(bro_follower_a == NULL)
        return ret = bro_follower_b;
    if(bro_follower_b == NULL)
        return ret = bro_follower_a;

    if(bro_follower_a->degree< bro_follower_b->degree){
        lastest = bro_follower_a;
        ret = lastest;
        bro_follower_a = bro_follower_a->brother;
    }else{
        lastest = bro_follower_b;
        ret = lastest;
        bro_follower_b = bro_follower_b->brother;
    }

    while(bro_follower_a != NULL||bro_follower_b != NULL){
        if(bro_follower_a == NULL){
            lastest->brother = bro_follower_b;
            bro_follower_b = bro_follower_b->brother;
            lastest = lastest->brother;
        }else if(bro_follower_b == NULL){
            lastest->brother = bro_follower_a;
            bro_follower_a = bro_follower_a->brother;
            lastest = lastest->brother;
        }else if(bro_follower_a->degree < bro_follower_b->degree){
            lastest->brother = bro_follower_a;
            bro_follower_a = bro_follower_a->brother;
            lastest = lastest->brother;
        }else{
            lastest->brother = bro_follower_b;
            bro_follower_b = bro_follower_b->brother;
            lastest = lastest->brother;
        }
    }

    lastest->brother = NULL;

    return ret;


}

heap_b* binomial_merge (heap_b** a, heap_b** b){
    binomial_node* after;
    binomial_node* x;
    binomial_node* before;
    

    heap_b* ret = create_heap();
    if(ret == NULL)
        return NULL;

    ret->nodes = NULL;
    ret->nb_head = (*a)->nb_head + (*b)->nb_head;
    binomial_node* linked_list = convert_binomial_heap_linked_list(*a, *b);
    before = NULL;
    x = linked_list;
    if(x != NULL)
        after = linked_list->brother;
    else
        after = NULL;
    
    ret->nodes = x;

    while(after!=NULL){
       if(after->brother != NULL){
           if(after->degree != x->degree){
               before = x;
               x = after;
               after = after->brother;
            }else{
                if(after->brother->degree == after->degree){
                    before = x;
                    x = after;
                    after = after->brother;
                }else{
                    if(x->key <= after->key){
                        x->brother = after->brother;
                        binomial_bind(after,x);
                        after = x->brother;

                        ret->nb_head--;
                    }else{
                        binomial_bind(x,after);
                        x = after;
                        after = after->brother;

                        ret->nb_head--;
                    }
                }
            }
       }else{
           if(x->degree == after->degree){
               if(x->key<=after->key){
                   x->brother = after->brother;
                   binomial_bind(after,x);
                   after = x->brother;

                   ret->nb_head--;
               }else{
                   binomial_bind(x,after);
                   x = after;
                   after = after->brother;

                   ret->nb_head--;
               }
           }else{
               before = x;
               x = after;
               after = after->brother;
           }
       }

       if(before == NULL)
           ret->nodes = x;
    }

    free(*a);
    free(*b);
    *a = NULL;
    *b = NULL;
    return ret;

}

void destroy_heap(heap_b *h){
    destroy_heap_nodes(&(h->nodes));
    free(h);
}

void destroy_heap_nodes(binomial_node **b){
   destroy_brothers(*b);
   *b=NULL;

}

void destroy_brothers(binomial_node *b){
    binomial_node *child_buffer = NULL;
    binomial_node *brother_buffer = NULL;
    while(b != NULL){
        brother_buffer = b->brother;
        child_buffer = b->child;
        free(b);
        destroy_childs(child_buffer);
        b = brother_buffer;

    }
}
void destroy_childs(binomial_node *b){
    binomial_node *brother_buffer = NULL;
    binomial_node *child_buffer = NULL;
    while(b != NULL){
        brother_buffer = b->brother;
        child_buffer = b->child;
        free(b);
        destroy_brothers(brother_buffer);
        b = child_buffer;
    }
}



binomial_node* reverse_node_order(binomial_node* bn){
    if(bn == NULL)
        return NULL;
    binomial_node** node_stack;
    int stack_index = 0; 
    binomial_node* brother_follower = bn;
    node_stack = 
        (binomial_node**) malloc ((bn->degree)*sizeof(binomial_node*));

    if(node_stack == NULL)
        return NULL;

    while(brother_follower != NULL){
        node_stack[stack_index] = brother_follower;

        brother_follower = brother_follower->brother;
        stack_index++;
    }

    
    for(int i = stack_index-1; i>=0; i--){
        if(i > 0)
            node_stack[i]->brother = node_stack[i-1];
        else
            node_stack[i]->brother = NULL;

    }

    return node_stack[stack_index-1];


}
int extract_min_key(heap_b** h, int* ret){
    heap_b* min_heap;
    
    binomial_node* brother_follower;
    binomial_node* before_brother;
    binomial_node* min_follower;
    binomial_node* before_min;

    heap_b* merged_heap;
    if(ret == NULL || h == NULL)
        return -1; // paramétres invalides
    
    min_heap = create_heap();
    if(min_heap == NULL)
        return -2; //Erreur d'initialisation

    brother_follower = (*h)->nodes;
    before_brother = NULL;

    min_follower = brother_follower;
    before_min = NULL;  /* si le noeud minimal est le premier alors
                           le noeud d'avant est inexistant donc 
                           ref à NULL*/

    if(brother_follower == NULL)
        return -3;
    
    *ret = brother_follower->key;

    while(brother_follower != NULL){
       if(brother_follower->key < *ret){
            
            *ret = brother_follower->key;

            min_follower = brother_follower;
            before_min = before_brother;
       }

       before_brother = brother_follower;
       brother_follower = brother_follower->brother;
    }

    min_heap->nodes = reverse_node_order(min_follower->child);
    min_heap->nb_head = min_follower->degree;

    /*on retire min_follower du tas en parametre*/
    if(before_min == NULL){
        /* cas ou le noeud min est le premier*/
        (*h)->nodes = min_follower->brother;
    }else{
       before_min->brother = min_follower->brother;
    }
    
    (*h)->nb_head --;

     merged_heap = binomial_merge(h, &min_heap);
     if(merged_heap == NULL){
        (*h)->nb_head++;
        before_min->brother = min_follower;
        return -3; //Erreur dans la fusion
     }
        
     *h = merged_heap;
     return 0;
}

void show_heap(heap_b* h){
    printf("heap id : %p \n\n", (void*)h);
    int i = 0;
    int i_print = 0;

    binomial_node** stack = (binomial_node**) malloc(1000000*sizeof(binomial_node*));

    stack[0] = h->nodes;
    binomial_node* stack_node = NULL;
    while(i >=0 && stack[0] != NULL){
        
        i_print ++;
        stack_node = stack[i];
        stack[i] = NULL;
        i--;

        printf("iter : %d \n", i_print);
        printf("key : %d \n", stack_node->key);

        if(stack_node->brother != NULL){
            i++;
            stack[i] = stack_node->brother;
        }

        if(stack_node->child != NULL){
            i++;
            stack[i] = stack_node->child;
        }

    }

    free(stack);

}
