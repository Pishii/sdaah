#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <stddef.h>
#include <math.h>
#include "structures/tas_binaire_dynamique.h"

tas_binaire_dynamique_t* creerTasBinaire(int n)
{
	tas_binaire_dynamique_t* ret;
	ret = (tas_binaire_dynamique_t*) malloc(sizeof(tas_binaire_dynamique_t));
	
    if(ret != NULL)
	{ 
	    ret->tree= arraylist_create();
		ret->tree->data[0] = n;
		ret->tree->size++;
		for(int i=1;i<ret->tree->capacity;i++)
		{
			ret->tree->data[i] = 2147483647;
		}
	}
    return ret;
}

void detruireTasBinaire(tas_binaire_dynamique_t* t)
{
	if( t != NULL )
	{
    	arraylist_destroy(t->tree);
   	}
   	free(t);
}

int pere(int i)
{	
	if(i>0) 
		return floor(i/2);
	else 
	{
		fprintf(stderr, "indice nul ou négatif \n");
     	exit(EXIT_FAILURE);
	}
}
int gauche(int i)
{
	return 2*i+1;
}
int droite(int i )
{
	return 2*i+2;
}

void entasser(int i, tas_binaire_dynamique_t *t)
{
	int g,d,min;
	g = gauche(i);
	d = droite(i);
    min = i;
	if ((g<= t->tree->size)&&(t->tree->data[g] <  t->tree->data[i]))
	{
		min = g;
	}
	if ((d<= t->tree->size)&&(t->tree->data[d] < t->tree->data[i]))
	{
	    if(t->tree->data[d]< t->tree->data[min])
		    min	= d;
	} 
	
	
	if (min != i) 
	{
		int a;
		a = t->tree->data[i];
		t->tree->data[i] = t->tree->data[min];
		t->tree->data[min] = a;
		if (min != 0)
		{
		    entasser(min,t);
		}
	}
	
}

int minimum(tas_binaire_dynamique_t *t)
{
	return t->tree->data[0];
}

int extraireMin(tas_binaire_dynamique_t *t)
{
	int min;
	
	if(t->tree->size < 1)
	{
		fprintf(stderr, "Erreur taille négative \n");
     	exit(EXIT_FAILURE);
	}
	min = t->tree->data[0];
	t->tree->data[0] = t->tree->data[(t->tree->size)-1];
	t->tree->data[(t->tree->size)-1] = 2147483647;
	if(arraylist_do_we_need_to_reduce_capacity(t->tree))
	{
      	arraylist_reduce_capacity(t->tree);
   	}
	entasser(0, t);	
    t->tree->size--;
	return min;	
}

void diminuerCle(tas_binaire_dynamique_t *t, int i, int x)
{
	if (x <= t->tree->data[i])
	{
		t->tree->data[i] = x;
		if((i > 0)&&(t->tree->data[pere(i)] > t->tree->data[i]))
		{
			int a = t->tree->data[i];
			t->tree->data[i] = t->tree->data[(pere(i))];
			t->tree->data[pere(i)] = a;
		}
		i = pere(i);
		if(i>0)
			diminuerCle(t, i, x);
	}
}
void inserer(tas_binaire_dynamique_t *t, int k)
{
	  	if(arraylist_do_we_need_to_enlarge_capacity(t->tree))
	  	{
      		arraylist_enlarge_capacity(t->tree);
      		for(int i=t->tree->size;i<t->tree->capacity;i++)
	    	{
		    	t->tree->data[i] = 2147483647;
	    	}
   	 	}
		t->tree->size++;
		diminuerCle(t,t->tree->size-1,k);
}

void afficher(tas_binaire_dynamique_t *t)
{
	for(int i=0;i<t->tree->capacity;i++)
	{
	    if(t->tree->data[i] != 2147483647)
		    printf("%d ",t->tree->data[i]);
		else printf("X ");
	} 
	printf("\n");
}

