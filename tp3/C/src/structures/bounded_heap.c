#include "structures/bounded_heap.h"
#include <stdlib.h>
#include <stdio.h>

bounded_heap_t * bounded_heap_create(int capacity){
  bounded_heap_t * h = (bounded_heap_t *)malloc(sizeof(bounded_heap_t));
  h->tree = (int *)malloc(sizeof(int) * capacity);
  h->capacity = capacity;
  h->size = 0;
  return h;
}

void bounded_heap_destroy(bounded_heap_t * h){
  if( h != NULL ){
    free( h->tree );
    free( h );
  }
}

size_t bounded_heap_swap_counter(bounded_heap_t * h){
  return h->swap_counter;
}

inline void bounded_heap_swap(bounded_heap_t * h, size_t pos1, size_t pos2){
  int tmp;
  tmp = h->tree[pos1];
  h->tree[pos1] = h->tree[pos2];
  h->tree[pos2] = tmp;
}

int bounded_heap_max_child(bounded_heap_t * h, size_t pos){
  int left, right;
  if( 2*pos+1 >= h->size)
    return -1;
  left = h->tree[2*pos+1];
  if( 2*pos+2 >= h->size)
    return 2*pos+1;  
  right = h->tree[2*pos+2];
  return ( left > right )? 2*pos+1: 2*pos+2;
}

inline char bounded_heap_is_full(bounded_heap_t * h){
  return ( h->capacity == h->size) ? 1:0;
}

void bounded_heap_up_heap(bounded_heap_t * h){
  int pos = h->size - 1;
  int pos_parent = (pos-1)/2;
  while( pos > 0 &&
	 h->tree[pos] > h->tree[pos_parent] ){
    bounded_heap_swap(h, pos, pos_parent);
    pos = pos_parent;
    pos_parent = (pos-1)/2;
  }
}


void bounded_heap_add(bounded_heap_t * h, int x){
  if(!bounded_heap_is_full(h)){
    h->tree[h->size++] = x;
    bounded_heap_up_heap(h);
  } else{
    fprintf(stderr,"Heap is full, cannot add %d\n", x);
  }
}

void bounded_heap_down_heap(bounded_heap_t * h){
  size_t max_child_pos;
  size_t pos = 0;
  max_child_pos = bounded_heap_max_child(h, pos);
  while( max_child_pos>=0 &&
	 h->tree[max_child_pos] > h->tree[pos] ){
    bounded_heap_swap(h, pos, max_child_pos);
    pos = max_child_pos;
    max_child_pos = bounded_heap_max_child(h, pos);
  }
}

int bounded_heap_pop(bounded_heap_t * h){
  int result = -1;
  
  if( h->size > 0 ){
    result = h->tree[0];
    bounded_heap_swap(h, 0, h->size-1);
    h->size--;
    bounded_heap_down_heap(h);
  }
  return result;
}


