#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <stddef.h>
#include <math.h>
#include "structures/tas_binaire_fixe.h"


tas_binaire_fixe_t* creerTasBinaire(int n,int cap)
{
	tas_binaire_fixe_t* ret;
	ret = (tas_binaire_fixe_t*) malloc(sizeof(tas_binaire_fixe_t));
	
    
    if(ret != NULL)
	{ 
	    ret->tree=NULL;
	    ret->size = 1;
		ret->capacity = cap; 
		ret->tree = (int*) malloc(sizeof(int)*ret->capacity);
		ret->tree[0] = n;
		for(int i=1;i<ret->capacity;i++)
		{
			ret->tree[i] = 2147483647;
		}
	}
	
    return ret;
}

void detruireTasBinaire(tas_binaire_fixe_t* t)
{
	if( t != NULL )
	{
    	if( t->tree != NULL )
    	  free( t->tree );
   	}
   	free(t);
}

int pere(int i)
{	
	if(i>0) 
		return floor(i/2);
	else 
	{
		fprintf(stderr, "indice nul ou négatif \n");
     	exit(EXIT_FAILURE);
	}
}
int gauche(int i)
{
	return 2*i+1;
}
int droite(int i )
{
	return 2*i+2;
}

void entasser(int i, tas_binaire_fixe_t *t)
{
	int g,d,min;
	g = gauche(i);
	d = droite(i);
	min = i;
	if ((g<= t->size)&&(t->tree[g] < t->tree[i]))
	{
		min = g;
	}
	if ((d<= t->size)&&(t->tree[d] < t->tree[i]))
	{
	    if(t->tree[d]< t->tree[min])
		    min	= d;
	} 

	if (min != i) 
	{
		int a;
		a = t->tree[i];
		t->tree[i] = t->tree[min];
		t->tree[min] = a;
		if (min != 0)
		{
		    entasser(min,t);
		}
	}
}

int minimum(tas_binaire_fixe_t *t)
{
	return t->tree[0];
}

int extraireMin(tas_binaire_fixe_t *t)
{
	int min;
	
	if(t->size < 1)
	{
		fprintf(stderr, "Erreur taille négative \n");
     	exit(EXIT_FAILURE);
	}
	min = t->tree[0];
	t->tree[0] = t->tree[(t->size)-1];
	t->tree[(t->size)-1] = 2147483647;
	t->size--;
	entasser(0, t);	
	return min;	
}

void diminuerCle(tas_binaire_fixe_t *t, int i, int x)
{
	if (x <= t->tree[i])
	{
		t->tree[i] = x;
		if((i > 0)&&(t->tree[pere(i)] > t->tree[i]))
		{
			int a = t->tree[i];
			t->tree[i] = t->tree[(pere(i))];
			t->tree[pere(i)] = a;
		}
		i = pere(i);
		if(i>0)
			diminuerCle(t, i, x);
	}
}
void inserer(tas_binaire_fixe_t *t, int k)
{
	if(t->size<t->capacity)
	{
		t->size++;
		diminuerCle(t,t->size-1,k);
	}
}

void afficher(tas_binaire_fixe_t *t)
{
	for(int i=0;i<t->capacity;i++)
	{
	    if(t->tree[i] != 2147483647)
		    printf("%d ",t->tree[i]);
		else printf("X ");
	} 
	printf("\n");
}
