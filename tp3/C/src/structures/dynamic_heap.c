#include "structures/dynamic_heap.h"
#include <stdlib.h>


dynamic_heap_t * dynamic_heap_create(){
  dynamic_heap_t * h = (dynamic_heap_t *)malloc(sizeof(dynamic_heap_t));
  h->tree = arraylist_create();
  h->swap_counter = 0;
  return h;
}

void dynamic_heap_destroy(dynamic_heap_t * h){
  if(h != NULL){
    arraylist_destroy(h->tree);
    free(h);
  }
}

size_t dynamic_heap_swap_counter(dynamic_heap_t * h){
  return h->swap_counter;
}

inline void dynamic_heap_swap(dynamic_heap_t * h, size_t pos1, size_t pos2){
  arraylist_swap(h->tree, pos1, pos2);
  h->swap_counter++;
}


int dynamic_heap_max_child(dynamic_heap_t * h, size_t pos){
  int left, right;
  left = arraylist_get(h->tree, 2*pos+1);
  if(left == -1)
    return -1;
  right = arraylist_get(h->tree, 2*pos+2);
  if(right == -1)
    return 2*pos+1;
  return ( left > right )? 2*pos+1: 2*pos+2;
}

void dynamic_heap_up_heap(dynamic_heap_t * h){
  int pos = arraylist_size(h->tree) - 1;
  int pos_parent = (pos-1)/2;
  while( pos > 0 &&
	 arraylist_get(h->tree, pos) > arraylist_get(h->tree, pos_parent) ){
    dynamic_heap_swap(h, pos, pos_parent);
    pos = pos_parent;
    pos_parent = (pos-1)/2;
  }
}

void dynamic_heap_add(dynamic_heap_t * h, int x){
  h->swap_counter = 0;
  arraylist_append(h->tree, x);
  dynamic_heap_up_heap(h);
}

void dynamic_heap_down_heap(dynamic_heap_t * h){
  size_t max_child_pos;
  size_t pos = 0;
  max_child_pos = dynamic_heap_max_child(h, pos);
  while( max_child_pos >= 0 &&
	 arraylist_get(h->tree, max_child_pos)  > arraylist_get(h->tree, pos) ){
    dynamic_heap_swap(h, pos, max_child_pos);
    pos = max_child_pos;
    max_child_pos = dynamic_heap_max_child(h, pos);
  }
}

int dynamic_heap_pop(dynamic_heap_t * h){
  int size = arraylist_size(h->tree);
  int result = -1;
  h->swap_counter = 0;
  if( size > 0 ){
    result = arraylist_get(h->tree, 0);
    dynamic_heap_swap(h, 0, size-1);
    arraylist_pop_back(h->tree);
    dynamic_heap_down_heap(h);
  }
  return result;
}

