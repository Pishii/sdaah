#include<stdio.h>
#include <time.h>
#include<stdlib.h>
#include "structures/dynamic_heap.h"
#include "analysis/analyzer.h"


int main(int argc, char ** argv){
  int i;
  // Tas dynamique.
  dynamic_heap_t * dh = dynamic_heap_create();
  // Analyse du temps pris par les opérations.
  analyzer_t * time_analysis = analyzer_create();
  // Analyse du nombre de copies faites par les opérations.
  analyzer_t * copy_analysis = analyzer_create();
  // Mesure de la durée d'une opération.
  struct timespec before, after;
  clockid_t clk_id = CLOCK_REALTIME;
  
  for(i = 0; i < 1000000 ; i++){
    // Ajout d'un élément et mesure du temps pris par l'opération.    
    clock_gettime(clk_id, &before);
    dynamic_heap_add(dh, i);
    clock_gettime(clk_id, &after);

    // Enregistrement du temps pris par l'opération
    analyzer_append(time_analysis, after.tv_nsec - before.tv_nsec);
    // Enregistrement du nombre de copies efféctuées par l'opération.
    // S'il y a eu réallocation de mémoire, il a fallu recopier tout le tableau.
    analyzer_append(copy_analysis, dynamic_heap_swap_counter(dh) );
  }

  // Affichage de quelques statistiques sur l'expérience.
  fprintf(stderr, "Total cost: %lf\n", get_total_cost(time_analysis));
  fprintf(stderr, "Average cost: %lf\n", get_average_cost(time_analysis));
  fprintf(stderr, "Variance: %lf\n", get_variance(time_analysis));
  fprintf(stderr, "Standard deviation: %lf\n", get_standard_deviation(time_analysis));

  // Sauvegarde les données de l'expérience.
  save_values(time_analysis, "../plots/dynamic_heap_time_c.plot");
  save_values(copy_analysis, "../plots/dynamic_heap_swap_c.plot");

  // Nettoyage de la mémoire avant la sortie du programme
  dynamic_heap_destroy(dh);
  analyzer_destroy(time_analysis);
  analyzer_destroy(copy_analysis);
  return EXIT_SUCCESS;
}
