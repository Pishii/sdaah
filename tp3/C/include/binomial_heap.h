#include <stddef.h>

typedef struct binomial_heap_s{
    binomial_tree_t* tree_list;
        
}binomial_heap_t;

typedef struct binomial_tree_s{
    int node;
    int number_element;
    binomial_tree_t* father;
    binomial_tree_t* brother;

}binomial_tree_t;


