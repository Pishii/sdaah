#ifndef __TAS_BINAIRE_FIXE_H__
#define __TAS_BINAIRE_FIXE_H__

#include <stddef.h>

typedef struct tas_binaire_fixe_s{
	int * tree;
	int size;
	int capacity;
	
} tas_binaire_fixe_t;

tas_binaire_fixe_t* creerTasBinaire(int n,int cap);
void detruireTasBinaire(tas_binaire_fixe_t* t);
int pere(int i);
int gauche(int i);
int droite(int i);
void entasser(int i, tas_binaire_fixe_t *t);
int minimum(tas_binaire_fixe_t *t);
int extraireMin(tas_binaire_fixe_t *t);
void diminuerCle(tas_binaire_fixe_t *t, int i, int x);
void inserer(tas_binaire_fixe_t *t, int k);
void afficher(tas_binaire_fixe_t *t);

#endif