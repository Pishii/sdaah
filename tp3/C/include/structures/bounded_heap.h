#ifndef __BOUNDED_HEAP_H__
#define __BOUNDED_HEAP_H__

#include<stddef.h>

typedef struct bounded_heap_s{
  int * tree;
  int capacity;
  int size;
  size_t swap_counter;
} bounded_heap_t;

bounded_heap_t * bounded_heap_create(int capacity);
void bounded_heap_destroy(bounded_heap_t * h);
void bounded_heap_add(bounded_heap_t * h, int x);
int bounded_heap_pop(bounded_heap_t * h);
size_t bounded_heap_swap_counter(bounded_heap_t * h);
char bounded_heap_is_full(bounded_heap_t * h);


#endif
