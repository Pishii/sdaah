#ifndef __TAS_BINAIRE_DYN_H__
#define __TAS_BINAIRE_DYN_H__

#include <stddef.h>
#include "arraylist.h"

typedef struct tas_binaire_dynamique_s{
	arraylist_t * tree;
	
} tas_binaire_dynamique_t;

tas_binaire_dynamique_t* creerTasBinaire(int n);
void detruireTasBinaire(tas_binaire_dynamique_t* t);
int pere(int i);
int gauche(int i);
int droite(int i);
void entasser(int i, tas_binaire_dynamique_t *t);
int minimum(tas_binaire_dynamique_t *t);
int extraireMin(tas_binaire_dynamique_t *t);
void diminuerCle(tas_binaire_dynamique_t *t, int i, int x);
void inserer(tas_binaire_dynamique_t *t, int k);
void afficher(tas_binaire_dynamique_t *t);

#endif