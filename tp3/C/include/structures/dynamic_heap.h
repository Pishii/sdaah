#ifndef __DYNAMIC_HEAP_H__
#define __DYNAMIC_HEAP_H__

#include<stddef.h>
#include"arraylist.h"

typedef struct dynamic_heap_s{
  arraylist_t * tree;
  size_t swap_counter;
} dynamic_heap_t;

dynamic_heap_t * dynamic_heap_create();
void dynamic_heap_destroy(dynamic_heap_t * h);
void dynamic_heap_add(dynamic_heap_t * h, int x);
int dynamic_heap_pop(dynamic_heap_t * h);
size_t dynamic_heap_swap_counter(dynamic_heap_t * h);

#endif
