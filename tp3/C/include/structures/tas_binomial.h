#include <stddef.h>

typedef struct binomial_node_s binomial_node;
typedef struct binomial_node_s{
    binomial_node *father;
    binomial_node *child;
    binomial_node *brother;
    void *content;
    int key;

    int degree;
} binomial_node;

typedef struct heap_b_t{
    binomial_node* nodes;
    int nb_head;
} heap_b;


heap_b* create_heap();

int insert_binomial_element(void *e, int key, heap_b **h);

void* extract_min(heap_b** h);
int extract_min_key(heap_b** h, int* ret);

void* show_min(heap_b* h);
void show_heap(heap_b* h);
void destroy_heap(heap_b* h);

/*ATTENTION LES DEUX TAS SONT DETRUITS PENDANT LA FUSION
 * Si la fusion echoue les deux tas sont laissé intact*/
heap_b* binomial_merge(heap_b** a, heap_b** b);
