#include <stddef.h>

typedef struct heap_b_t{
    int* nodes;
    int capacity;
} heap_b;

heap_b* create_heap(int capacity);
int insert_element(int e, heap_b* h);
int delete_element(int e, heap_b* h);
int pop_head(heap_b* h);
void destroy_heap(heap_b* h);

