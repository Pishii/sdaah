# Projet SDA M1

## But du projet
Il s'agit d'une suite de travaux pratiques dont le but est d'implémenter différentes structures de données

Le but est de trouver les implémentations les plus efficaces en terme de gestion mémoire et de temps d'éxécution.

# Comment compiler les differentes partie du projet
## TP1
Le tp1 se situe dans le répertoire tp1.
faire make
puis ./arraylist_analysis

Les graphes sont des le dossier plots.

## TP2
Le tp2 se décompose en deux sous-parties
La premiere se situe dans le répertoire tp2
La deuxieme dans le répértoire tp2-sqrt

les consigne de compilation sont les même que dans le tp1

les graphes sont à la racine des deux dossier

## TP3
Les fichier dans le dossier source doivent compilés séparément

* gcc -c -g -Iinclude src/structures/*
* gcc -c -g -Iinclude src/analysis/*
* gcc -g -Iinclude test/test_stat.c *.o -lm
* gcc -g -Iinclude test/test_tas_binaire_dynamique.c *.o -lm
* gcc -g -Iinclude test/test_tas_binaire_statique.c *.o -lm

les graphes sont dans C/imgplot
## TP4
Consigne identiques que pour le tp3

* gcc -c -g -Iinclude src/structures/*
* gcc -c -g -Iinclude src/analysis/*
* gcc -g -Iinclude test/test_avl.c *.o -lm
* gcc -g -Iinclude test/test_b_arbre.c *.o -lm

les graphes sont dans C/imgplot

# Rapport
Le rapport est dans la racine du projet. Il se nomme Rapport_SDA.pdf

# Contributeurs
* Yacine BELMAATI CHERKAOUI (11513398)
* Rémi PHYU THANT THAR (11509205)

