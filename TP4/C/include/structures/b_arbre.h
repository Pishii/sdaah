#ifndef __B_ARBRE_H
#define __B_ARBRE_H

#include <stdio.h>
#include <stdlib.h>

#define degre 2
#define TRUE 1
#define FALSE 0

typedef unsigned char bool;

typedef struct noeud 
{
	int tab_cle[2*degre - 1];
	struct noeud *tab_noeud_enfant[2*degre];
	int nombre_cle_conservees;
	bool feuille;
} noeud_t;

typedef struct
{
    noeud_t * racine;
} b_arbre_t;

typedef struct {
	noeud_t *noeud;
	int cle;
	bool trouve;
} resultat_recherche_t;

b_arbre_t * creer_b_arbre();
noeud_t * creer_noeud();
void detruire_noeud(noeud_t * n);
void detruire_b_arbre(b_arbre_t* b);
resultat_recherche_t * creer_resultat();
resultat_recherche_t * rechercher_cle(noeud_t * n, int x);
void partager_enfant_b_arbre(noeud_t * noeud, int i, noeud_t *noeud_enfant);
void inserer_b_arbre_incomplet(noeud_t *n, int cle);
void inserer(int cle, b_arbre_t *b);
void merge_children(noeud_t *racine, int index, noeud_t *child1, noeud_t *child2);
static void BTreeBorrowFromLeft(noeud_t *racine, int index, noeud_t *leftPtr, noeud_t *curPtr);
static void BTreeBorrowFromRight(noeud_t *racine, int index, noeud_t *rightPtr, noeud_t *curPtr);
static void BTreeDeleteNoNone(int X, noeud_t *racine);
void *supprimer(int key, b_arbre_t *b);
void display(noeud_t *ptr, int blanks);
void eatline(void);

static int BTreeGetLeftMax(noeud_t *T);
static int BTreeGetRightMin(noeud_t *T);

#endif