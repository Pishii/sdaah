#ifndef __AVL_H
#define __AVL_H

#include <stddef.h>

struct node
{
    int key;
    int data;    

    int height;

    struct node* left;
    struct node* right;
};

typedef struct node node;

node* new_node(int key, int data);
int max(int a, int b);
int height(node* p);
void recalc(node* p);
node* rotate_right(node* p);
node* rotate_left(node* p);
node* balance(node* p);
node* search(node* p, int key);
node* insert(node* p, int key, int data);
node* find_min(node* p);
node* remove_min(node* p);
node* remove_item(node* p, int key);
void free_tree(node* p);

#endif