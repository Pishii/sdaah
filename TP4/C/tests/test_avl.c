#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include "structures/avl.h"
#include "analysis/analyzer.h"

int tab[1000];
int main(){
    analyzer_t* ajout_croissant_avl = analyzer_create();
    analyzer_t* ajout_suppr_aleatoire_avl = analyzer_create();
    analyzer_t* ajout_aleatoire_avl = analyzer_create();
	analyzer_t* ajout_suppr_avl = analyzer_create();

    node * avl_ajout_croissant=  new_node(0, 0);
	node * avl_ajout_aleatoire=  new_node(0, 0);
	node * avl_ajout_suppr=  new_node(0, 0);
    node* avl_ajout_suppr_aleatoire = new_node(0,0);
	 
    struct timespec after, before;
    clockid_t clk_id = CLOCK_REALTIME;
    
    srand(11509205); 

	for(int i = 0; i<1000; i++)
    {
        tab[i] = rand();
    }
    printf("Scénario avec ajout croissant\n");	
    for(int i = 0; i < 1000; i++){
        clock_gettime(clk_id, &before); 
        avl_ajout_croissant = insert(avl_ajout_croissant,i,i);
        clock_gettime(clk_id, &after);
        analyzer_append(ajout_croissant_avl, after.tv_nsec - before.tv_nsec);
    }
    printf("temps total : %f \n", get_total_cost(ajout_croissant_avl));
    //printf("temps amorti : %f \n", get_amortized_cost(ajout_croissant_avl));
    printf("temps moyen : %f \n", get_average_cost(ajout_croissant_avl));
    printf("variance : %f \n", get_variance(ajout_croissant_avl));
    printf("écart_type : %f \n", get_standard_deviation(ajout_croissant_avl));
    save_values(ajout_croissant_avl, "./ajout_croissant_avl.plot");
	free_tree(avl_ajout_croissant);
	analyzer_destroy(ajout_croissant_avl);

    printf("Scénario avec ajout aléatoire\n");
    for(int i = 0; i<1000; i++){
        clock_gettime(clk_id, &before); 
        avl_ajout_aleatoire= insert(avl_ajout_aleatoire,tab[i],tab[i]);
        clock_gettime(clk_id, &after);
        analyzer_append(ajout_aleatoire_avl, after.tv_nsec - before.tv_nsec);
    }
        
    printf("temps total : %f \n", get_total_cost(ajout_aleatoire_avl));
    //printf("temps amorti : %f \n", get_amortized_cost(ajout_aleatoire_avlinaire));
    printf("temps moyen : %f \n", get_average_cost(ajout_aleatoire_avl));
    printf("variance : %f \n", get_variance(ajout_aleatoire_avl));
    printf("écart_type : %f \n", get_standard_deviation(ajout_aleatoire_avl));
    save_values(ajout_aleatoire_avl, "./ajout_aleatoire_avl.plot");
	free_tree(avl_ajout_aleatoire);
	analyzer_destroy(ajout_aleatoire_avl);
	
	printf("Scénario avec ajout croissant et suppression\n");
    for(int i = 0; i<1000; i++){
        clock_gettime(clk_id, &before); 
        avl_ajout_suppr= insert(avl_ajout_suppr,i,i);
        clock_gettime(clk_id, &after);
        analyzer_append(ajout_suppr_avl, after.tv_nsec - before.tv_nsec);
    }
    for(int i = 0; i<1000; i++){
        clock_gettime(clk_id, &before); 
        avl_ajout_suppr= remove_item(avl_ajout_suppr,i);
        clock_gettime(clk_id, &after);
        analyzer_append(ajout_suppr_avl, after.tv_nsec - before.tv_nsec);
    }
        
    printf("temps total : %f \n", get_total_cost(ajout_suppr_avl));
    //printf("temps amorti : %f \n", get_amortized_cost(ajout_suppr_avl));
    printf("temps moyen : %f \n", get_average_cost(ajout_suppr_avl));
    printf("variance : %f \n", get_variance(ajout_suppr_avl));
    printf("écart_type : %f \n", get_standard_deviation(ajout_suppr_avl));
    save_values(ajout_suppr_avl, "./ajout_suppr_avl.plot");
	free_tree(avl_ajout_suppr);
	analyzer_destroy(ajout_suppr_avl);
    
    
    printf("Scénario avec ajout et suppression aléatoire\n");
    
      
    for(int i = 0; i<1000; i++){
        clock_gettime(clk_id, &before); 
        avl_ajout_suppr_aleatoire= insert(avl_ajout_suppr_aleatoire,tab[i],tab[i]);
        clock_gettime(clk_id, &after);
        analyzer_append(ajout_suppr_aleatoire_avl, after.tv_nsec - before.tv_nsec);
    }
    for(int i = 0; i<1000; i++){
        clock_gettime(clk_id, &before); 
        avl_ajout_suppr_aleatoire= remove_item(avl_ajout_suppr_aleatoire, tab[i]);
        clock_gettime(clk_id, &after);
        analyzer_append(ajout_suppr_aleatoire_avl, after.tv_nsec - before.tv_nsec);
    }
        
    printf("temps total : %f \n", get_total_cost(ajout_suppr_aleatoire_avl));
    //printf("temps amorti : %f \n", get_amortized_cost(ajout_suppr_aleatoire_avl));
    printf("temps moyen : %f \n", get_average_cost(ajout_suppr_aleatoire_avl));
    printf("variance : %f \n", get_variance(ajout_suppr_aleatoire_avl));
    printf("écart_type : %f \n", get_standard_deviation(ajout_suppr_aleatoire_avl));
    save_values(ajout_suppr_aleatoire_avl, "./ajout_suppr_aleatoire_avl.plot");
	free_tree(avl_ajout_suppr_aleatoire);
	analyzer_destroy(ajout_suppr_aleatoire_avl);
    return 0;
}
