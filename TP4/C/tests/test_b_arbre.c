#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include "structures/b_arbre.h"
#include "analysis/analyzer.h"

int tab[1000];
int main(){
    analyzer_t* ajout_croissant_b = analyzer_create();
    analyzer_t* ajout_suppr_aleatoire_b = analyzer_create();
    analyzer_t* ajout_aleatoire_b = analyzer_create();
	analyzer_t* ajout_suppr_b = analyzer_create();

    b_arbre_t * b_ajout_croissant=  creer_b_arbre();
    b_arbre_t * b_ajout_suppr_aleatoire=  creer_b_arbre();
	b_arbre_t * b_ajout_aleatoire=  creer_b_arbre();
	b_arbre_t * b_ajout_suppr=  creer_b_arbre();
	 
    struct timespec after, before;
    clockid_t clk_id = CLOCK_REALTIME;
    
    srand(11509205); 
    
    for(int i = 0; i < 1000; i++){
        tab[i] = rand();
    }    
    
    for(int i = 0; i < 1000; i++){
        clock_gettime(clk_id, &before); 
        inserer(i, b_ajout_croissant);
        clock_gettime(clk_id, &after);
        nalyzer_append(ajout_croissant_b, after.tv_nsec - before.tv_nsec);
    }
    printf("Scénario avec ajout croissant\n");	
    for(int i = 0; i < 1000; i++){
        clock_gettime(clk_id, &before); 
        inserer(i, b_ajout_croissant);
        clock_gettime(clk_id, &after);
        analyzer_append(ajout_croissant_b, after.tv_nsec - before.tv_nsec);
    }
    printf("temps total : %f \n", get_total_cost(ajout_croissant_b));
    printf("temps amorti : %f \n", get_amortized_cost(ajout_croissant_b));
    printf("temps moyen : %f \n", get_average_cost(ajout_croissant_b));
    printf("variance : %f \n", get_variance(ajout_croissant_b));
    printf("écart_type : %f \n", get_standard_deviation(ajout_croissant_b));
    save_values(ajout_croissant_b, "./ajout_croissant_b.plot");
    display(b_ajout_croissant->racine,0);
	detruire_b_arbre(b_ajout_croissant);
	analyzer_destroy(ajout_croissant_b);

    printf("Scénario avec ajout aléatoire\n");
    for(int i = 0; i<1000; i++){
        clock_gettime(clk_id, &before); 
        inserer(tab[i], b_ajout_aleatoire);
        clock_gettime(clk_id, &after);
        analyzer_append(ajout_aleatoire_b, after.tv_nsec - before.tv_nsec);
    }
        
    printf("temps total : %f \n", get_total_cost(ajout_aleatoire_b));
    printf("temps amorti : %f \n", get_amortized_cost(ajout_aleatoire_binaire));
    printf("temps moyen : %f \n", get_average_cost(ajout_aleatoire_b));
    printf("variance : %f \n", get_variance(ajout_aleatoire_b));
    printf("écart_type : %f \n", get_standard_deviation(ajout_aleatoire_b));
    save_values(ajout_aleatoire_b, "./ajout_aleatoire_b.plot");
    display(b_ajout_aleatoire->racine,0);
	detruire_b_arbre(b_ajout_aleatoire);
	analyzer_destroy(ajout_aleatoire_b);
	
	printf("Scénario avec ajout croissant et suppression\n");
    for(int i = 0; i<1000; i++){
        clock_gettime(clk_id, &before); 
        inserer(i, b_ajout_suppr);
        clock_gettime(clk_id, &after);
        analyzer_append(ajout_suppr_b, after.tv_nsec - before.tv_nsec);
    }
    for(int i = 0; i<1000; i++){
        clock_gettime(clk_id, &before); 
        supprimer(i, b_ajout_suppr);
        clock_gettime(clk_id, &after);
        analyzer_append(ajout_suppr_b, after.tv_nsec - before.tv_nsec);
    }
        
    printf("temps total : %f \n", get_total_cost(ajout_suppr_b));
    printf("temps amorti : %f \n", get_amortized_cost(ajout_suppr_b));
    printf("temps moyen : %f \n", get_average_cost(ajout_suppr_b));
    printf("variance : %f \n", get_variance(ajout_suppr_b));
    printf("écart_type : %f \n", get_standard_deviation(ajout_suppr_b));
    save_values(ajout_suppr_b, "./ajout_suppr_b.plot");
    display(b_ajout_suppr->racine,0);
	detruire_b_arbre(b_ajout_suppr);
	analyzer_destroy(ajout_suppr_b);
    
    
    printf("Scénario avec ajout et suppression aléatoire\n");
    for(int i = 0; i<1000; i++){
        clock_gettime(clk_id, &before); 
        inserer(tab[i], b_ajout_suppr_aleatoire);
        clock_gettime(clk_id, &after);
        analyzer_append(ajout_suppr_aleatoire_b, after.tv_nsec - before.tv_nsec);
    }
    for(int i = 0; i<1000; i++){
        clock_gettime(clk_id, &before); 
        b_ajout_suppr_aleatoire->racine= supprimer(tab[i], b_ajout_suppr_aleatoire);
        clock_gettime(clk_id, &after);
        analyzer_append(ajout_suppr_aleatoire_b, after.tv_nsec - before.tv_nsec);
    }
        
    printf("temps total : %f \n", get_total_cost(ajout_suppr_aleatoire_b));
    printf("temps amorti : %f \n", get_amortized_cost(ajout_suppr_aleatoire_b));
    printf("temps moyen : %f \n", get_average_cost(ajout_suppr_aleatoire_b));
    printf("variance : %f \n", get_variance(ajout_suppr_aleatoire_b));
    printf("écart_type : %f \n", get_standard_deviation(ajout_suppr_aleatoire_b));
    save_values(ajout_suppr_aleatoire_b, "./ajout_suppr_aleatoire_b.plot");
    display(b_ajout_suppr_aleatoire->racine,0);
	detruire_b_arbre(b_ajout_suppr_aleatoire);
	analyzer_destroy(ajout_suppr_aleatoire_b);
    return 0;
}
