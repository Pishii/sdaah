/*src :
	https://gist.github.com/squiidz/996129e16d870a8244fcaa7010ee8b5c
	https://gist.github.com/yorickdewid/d86e14cb2f3929823841
	Introduction à l'algorithmique
*/

#include <stdio.h>
#include <stdlib.h>
#include "structures/b_arbre.h"

#define degre 2
#define TRUE 1
#define FALSE 0


b_arbre_t * creer_b_arbre()
{
    b_arbre_t* b_arbre;
    b_arbre = (b_arbre_t*) malloc(sizeof(b_arbre_t));
    b_arbre->racine = creer_noeud();
    return b_arbre;
}

void detruire_b_arbre(b_arbre_t* b)
{
	if( b != NULL )
	{
    	detruire_noeud(b->racine);
   	}
   	free(b);
}

noeud_t *creer_noeud()
{
	noeud_t * noeud = (noeud_t *)malloc(sizeof(noeud_t));

	// cle:0
	for(int i = 0;i < 2*degre - 1; i++)
	{
		noeud->tab_cle[i] = 0;
	}

	// tab_noeud_enfant
	for(int i = 0;i < 2*degre; i++)
	{
		noeud->tab_noeud_enfant[i] = NULL;
	}

	noeud->nombre_cle_conservees = 0;
	noeud->feuille = TRUE;

	return noeud;
}

void detruire_noeud(noeud_t * n)
{
	if( n != NULL )
	{
    	if(!n->feuille)
		{
			for(int i= 0; i< sizeof(n->tab_noeud_enfant)/sizeof(int); i++)
				detruire_noeud(n->tab_noeud_enfant[i]);
		}
   	}
   	free(n);
}

resultat_recherche_t *creer_resultat()
{
	resultat_recherche_t *ret = (resultat_recherche_t *)malloc(sizeof(resultat_recherche_t));
	if(!ret){
		printf("ERROR! Out of memory.");
		exit(0);
	}

	ret->noeud = NULL;
	ret->cle = 0;
	ret->trouve = FALSE;

	return ret;
}

resultat_recherche_t * rechercher_cle(noeud_t * n, int x)
{
    int i = 0;
   
    
	while((i <= n->nombre_cle_conservees) && (x > n->tab_cle[i]))
	{
	    i++;
	}
	if((i <= n->nombre_cle_conservees) && (x == n->tab_cle[i]))
	{
		resultat_recherche_t *resultat= creer_resultat();
		resultat->noeud = n;
		resultat->cle = i;
		resultat->trouve = TRUE;
		return resultat;
	}
	if(n->feuille)
	{
	    resultat_recherche_t *resultat= creer_resultat();
		resultat->noeud = n;
		resultat->trouve= FALSE;
		return resultat;
	}
	else
	{
		return rechercher_cle(n->tab_noeud_enfant[i],x);
	}
}

void partager_enfant_b_arbre(noeud_t * noeud, int i, noeud_t *noeud_enfant)
{
    int j;
	noeud_t * n = creer_noeud();
	n->feuille = noeud_enfant->feuille;
	n->nombre_cle_conservees = degre-1;

	for(j = 0;j < degre-1;j++)
	{
		n->tab_cle[j] = noeud_enfant->tab_cle[degre+j];
	}

	if(!noeud_enfant->feuille)
	{
		for(j = 0;j < degre;j++)
		{
			n->tab_noeud_enfant[j] = noeud_enfant->tab_noeud_enfant[degre+j];
		}
	}
	noeud_enfant->nombre_cle_conservees = degre-1;


	for(j = noeud->nombre_cle_conservees;j>=i;j--){
		noeud->tab_noeud_enfant[j+1] = noeud->tab_noeud_enfant[j];
	}

	noeud->tab_noeud_enfant[i] = n;

	for(j = noeud->nombre_cle_conservees; j>=i; j--){
		noeud->tab_cle[j] = noeud->tab_cle[j-1];
	}

	noeud->tab_cle[i-1] = noeud_enfant->tab_cle[degre-1];

	noeud->nombre_cle_conservees++;
}

 void inserer_b_arbre_incomplet(noeud_t *n, int cle)
 {
	int i = n->nombre_cle_conservees;
	
	if(n->feuille)
	{
		while(i>=1 && cle<n->tab_cle[i-1])
		{
			n->tab_cle[i] = n->tab_cle[i-1];
			i--;
		}
		n->tab_cle[i] = cle;
		n->nombre_cle_conservees++;
	}
	else
	{
		while(i>=1 && cle<n->tab_cle[i-1])
		{
			i--;
		}
		if(n->tab_noeud_enfant[i]->nombre_cle_conservees == 2*degre - 1)
		{
			partager_enfant_b_arbre(n, i+1, n->tab_noeud_enfant[i]);
			if(cle > n->tab_cle[i])
			{
				i++;
			}
		}
		inserer_b_arbre_incomplet(n->tab_noeud_enfant[i], cle);
	}
}

void inserer(int cle, b_arbre_t *b)
{
		noeud_t *racine = b->racine;
		if(racine->nombre_cle_conservees == 2*degre - 1)
		{ 
			noeud_t * nnoeud = creer_noeud();
			b->racine = nnoeud; 
			nnoeud->feuille = 0;
			nnoeud->nombre_cle_conservees = 0;
			nnoeud->tab_noeud_enfant[0] = racine;
			partager_enfant_b_arbre(nnoeud, 1, racine);
			inserer_b_arbre_incomplet(nnoeud, cle);
		}
		else
		{
			inserer_b_arbre_incomplet(b->racine, cle);
		}
}

void merge_children(noeud_t *racine, int index, noeud_t *child1, noeud_t *child2)
{
	child1->nombre_cle_conservees = 2*degre-1;
	int i;

	for(i=degre;i<2*degre-1;i++)
		child1->tab_cle[i] = child2->tab_cle[i-degre];
		
	child1->tab_cle[degre-1] = racine->tab_cle[index]; 
	
	if(0 == child2->feuille){
		for(i=degre;i<2*degre;i++)
			child1->tab_noeud_enfant[i] = child2->tab_noeud_enfant[i-degre];
	}

	for(i=index+1;i<racine->nombre_cle_conservees;i++){
		racine->tab_cle[i-1] = racine->tab_cle[i];
		racine->tab_noeud_enfant[i] = racine->tab_noeud_enfant[i+1];
	}
	racine->nombre_cle_conservees--;
	free(child2);
}

static void BTreeBorrowFromLeft(noeud_t *racine, int index, noeud_t *leftPtr, noeud_t *curPtr){
	curPtr->nombre_cle_conservees++;
	int i;
	for(i=curPtr->nombre_cle_conservees-1;i>0;i--)
		curPtr->tab_cle[i] = curPtr->tab_cle[i-1];
	curPtr->tab_cle[0] = racine->tab_cle[index];
	racine->tab_cle[index] = leftPtr->tab_cle[leftPtr->nombre_cle_conservees-1];
	if(0 == leftPtr->feuille)
		for(i=curPtr->nombre_cle_conservees;i>0;i--)
			curPtr->tab_noeud_enfant[i] = curPtr->tab_noeud_enfant[i-1];
	curPtr->tab_noeud_enfant[0] = leftPtr->tab_noeud_enfant[leftPtr->nombre_cle_conservees];
	leftPtr->nombre_cle_conservees--;
}

static void BTreeBorrowFromRight(noeud_t *racine, int index, noeud_t *rightPtr, noeud_t *curPtr){
	curPtr->nombre_cle_conservees++;
	curPtr->tab_cle[curPtr->nombre_cle_conservees-1] = racine->tab_cle[index];
	racine->tab_cle[index] = rightPtr->tab_cle[0];
	int i;
	for(i=0;i<rightPtr->nombre_cle_conservees-1;i++)
		rightPtr->tab_cle[i] = rightPtr->tab_cle[i+1];
	if(0 == rightPtr->feuille){
		curPtr->tab_noeud_enfant[curPtr->nombre_cle_conservees] = rightPtr->tab_noeud_enfant[0];
		for(i=0;i<rightPtr->nombre_cle_conservees;i++)
			rightPtr->tab_noeud_enfant[i] = rightPtr->tab_noeud_enfant[i+1];
	}
	rightPtr->nombre_cle_conservees--;
}

static void BTreeDeleteNoNone(int X, noeud_t *racine){
	int i;

	if(1 == racine->feuille){
		i=0;
		while( (i<racine->nombre_cle_conservees) && (X>racine->tab_cle[i])) //Find the index of X.
			i++;

		if(X == racine->tab_cle[i]){
			for(;i<racine->nombre_cle_conservees-1;i++)
				racine->tab_cle[i] = racine->tab_cle[i+1];
			racine->nombre_cle_conservees--;
		}
		else{
			printf("Node not found.\n");
			return ;
		}
	}
	else{  
		i = 0;
		noeud_t *prePtr = NULL, *nexPtr = NULL;

		while( (i<racine->nombre_cle_conservees) && (X>racine->tab_cle[i]) )
			i++;
		if( (i<racine->nombre_cle_conservees) && (X == racine->tab_cle[i]) ){ //Find it in this level.
			prePtr = racine->tab_noeud_enfant[i];
			nexPtr = racine->tab_noeud_enfant[i+1];
			if(prePtr->nombre_cle_conservees > degre-1){
				int aPrecursor = BTreeGetLeftMax(prePtr);
				racine->tab_cle[i] = aPrecursor;
				BTreeDeleteNoNone(aPrecursor,prePtr);
			}
			else
			if(nexPtr->nombre_cle_conservees > degre-1){

				int aSuccessor = BTreeGetRightMin(nexPtr);
				racine->tab_cle[i] = aSuccessor;
				BTreeDeleteNoNone(aSuccessor,nexPtr);
			}
			else{

				merge_children(racine,i,prePtr,nexPtr);
				BTreeDeleteNoNone(X,prePtr);
			}
		}
		else{ 
			prePtr = racine->tab_noeud_enfant[i];
			noeud_t *leftBro = NULL;
			if(i<racine->nombre_cle_conservees)
				nexPtr = racine->tab_noeud_enfant[i+1];
			if(i>0)
				leftBro = racine->tab_noeud_enfant[i-1];

			if(degre-1 == prePtr->nombre_cle_conservees){

				if( (leftBro != NULL) && (leftBro->nombre_cle_conservees > degre-1))
					BTreeBorrowFromLeft(racine,i-1,leftBro,prePtr);
				else 
				if( (nexPtr != NULL) && (nexPtr->nombre_cle_conservees > degre-1))
					BTreeBorrowFromRight(racine,i,nexPtr,prePtr);
				else if(leftBro != NULL){
					merge_children(racine,i-1,leftBro,prePtr);
					prePtr = leftBro;
				}
				else 
					merge_children(racine,i,prePtr,nexPtr);
			}

			BTreeDeleteNoNone(X,prePtr);
		}
	}
}

static int BTreeGetLeftMax(noeud_t *T){
	if(0 == T->feuille){
		return BTreeGetLeftMax(T->tab_noeud_enfant[T->nombre_cle_conservees]);
	}else{
		return T->tab_cle[T->nombre_cle_conservees-1];
	}
}

static int BTreeGetRightMin(noeud_t *T){
	if(0 == T->feuille){
		return BTreeGetRightMin(T->tab_noeud_enfant[0]);
	}else{
		return T->tab_cle[0];
	}
}

void *supprimer(int key, b_arbre_t *b)
{

		if(b->racine->nombre_cle_conservees == 1){
			noeud_t *child1 = b->racine->tab_noeud_enfant[0];
			noeud_t *child2 = b->racine->tab_noeud_enfant[1];
			if((!child1) && (!child2)){
				if((degre-1 == child1->nombre_cle_conservees) && (degre-1 == child2->nombre_cle_conservees)){
					
					merge_children(b->racine, 0, child1, child2);
					free(b->racine);
					BTreeDeleteNoNone(key, child1);
					return child1;
				}
			}
		}
		BTreeDeleteNoNone(key, b->racine);
}
void display(noeud_t *ptr, int blanks) 
{
        if (ptr) {
                int i;
                for (i = 1; i <= blanks; i++)
                        printf(" ");
                for (i = 0; i < ptr->nombre_cle_conservees; i++)
                        printf("%d ", ptr->tab_cle[i]);
                printf("\n");
                for (i = 0; i <= ptr->nombre_cle_conservees; i++)
                        display(ptr->tab_noeud_enfant[i], blanks + 10);
        }
}

void eatline(void) {
        char c;
        while ((c = getchar()) != '\n');
}

/*int main() 
{
        int key;
        int choice;
        b_arbre_t *b = creer_b_arbre();
        printf("Creation of B tree for M=%d\n", degre);
        while (1) {
                printf("1.Insert\n");
                printf("2.Delete\n");
                printf("4.Display\n");
                printf("5.Quit\n");
                printf("Enter your choice : ");
                scanf("%d", &choice); eatline();

                switch (choice) {
                case 1:
                        printf("Enter the key : ");
                        scanf("%d", &key); eatline();
                        inserer(key,b);
                        break;
                case 2:
                        printf("Enter the key : ");
                        scanf("%d", &key); eatline();
                        supprimer(key,b);
                        break;
                case 4:
                        printf("Btree is :\n");
                        display(b->racine, 0);
                        break;
                case 5:
                        exit(1);
                default:
                        printf("Wrong choice\n");
                        break;
                }
        }
        return 0;
}*/



